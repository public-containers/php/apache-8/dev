#!/bin/bash

set -e

#Fix SSH permissions
mkdir -p ~/.ssh
chmod 0700 ~/.ssh/  || :
chmod 0644 ~/.ssh/id_rsa.pub  || :
chmod 0600 ~/.ssh/id_rsa  || :
chmod 0600 ~/.ssh/authorized_keys  || :

service ssh start

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

docker-php-entrypoint  "$@"
