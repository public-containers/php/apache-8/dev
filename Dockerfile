FROM registry.gitlab.com/public-containers/php/apache-8/prod

# Dependencies
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qq \
        openssh-server \
        sudo \
        git \
        zip \
        unzip \
        -y

RUN docker-php-ext-install mysqli pdo pdo_mysql

# Composer
RUN curl -o /usr/local/bin/composer https://getcomposer.org/composer-stable.phar && \
    chmod +x /usr/local/bin/composer
RUN composer global require diazoxide/nardivan:dev-master

# Cleanup
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Entrypoint
COPY entrypoint.sh /usr/local/bin/docker-php_apache_8_dev-entrypoint.sh
RUN ["chmod", "+x", "/usr/local/bin/docker-php_apache_8_dev-entrypoint.sh"]
ENTRYPOINT ["docker-php_apache_8_dev-entrypoint.sh"]

# Command
CMD ["apache2-foreground"]
